class User
	include DataMapper::Resource
	include BCrypt

	property :id, Serial, :key => true
	property :username, String, :length => 3..50
	property :password, BCryptHash
	property :role, String, required: true, default: 'client'
	property :type, Discriminator

	def authenticate(attempted_password)
		if self.password == attempted_password
			true
		else
			false
		end
	end

	def in_role?(role)
		if self.role == role
			true
		else
			false
		end
	end

	has n, :hardware
	has n, :task
	has n, :logs
end

# class Client < User

# end

class Manager < User
	has n, :posts
end

# class Tech < User

# end

# class Tasks
# 	include DataMapper::Resource

# end

class Task
	include DataMapper::Resource

	property :id, Serial
	property :content, Text
	property :completed, Boolean, :required => true, :default => false
	property :name, String, :required => true
	property :assignet_to, String, :default => "Noone"
	property :created_at, DateTime
	property :updated_at, DateTime

	belongs_to :user
end

class Hardware
	include DataMapper::Resource

	property :id, Serial
	property :name, String
	property :type, String
	belongs_to :user
end

class Post
	include DataMapper::Resource

	property :id, Serial
	property :title, String
	property :content, Text
	property :created_at, DateTime

	belongs_to :manager
end

class Log
	include DataMapper::Resource

	property :id, Serial
	property :log_string, String
	property :date, DateTime

	belongs_to :user
end

DataMapper.finalize.auto_upgrade!

if User.count == 0
	@user = User.create(username: "John")
	@user.password = "root"
	@user.role = "admin"
	@user.save
end