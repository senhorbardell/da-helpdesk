# require 'sinatra'
# require 'haml'
# require 'data_mapper'
require 'bcrypt'
# require 'warden'
# require 'sinatra/flash'
# require 'sinatra/partial'
# require 'sass'
# require 'pg'

# require 'rack/flash'
# require 'rack-flash3'

require 'bundler'
# Bundler.require :default

class Helpdesk < Sinatra::Base
	
	configure :development do
		Bundler.require :default, :development
		DataMapper::setup(:default, "sqlite3://#{Dir.pwd}/dev.db")

		Compass.configuration do |config|
			config.project_path = File.dirname(__FILE__)
			config.sass_dir = 'views'
		end

		set :haml, {format: :html5}
		set :sass, Compass.sass_engine_options
	end

	configure :production do 
		Bundler.require :default, :production
		DataMapper::setup(:default, 'postgres://tyeliedmufacjm:ab-1qiDaf6WzNm-4H16mmLXzRP@ec2-79-125-25-99.eu-west-1.compute.amazonaws.com:5432/d90ct5keu17ac')
		
		Compass.configuration do |config|
			config.project_path = File.dirname(__FILE__)
			config.sass_dir = 'views'
			config.output_style = :compressed

			set :haml, {format: :html5}
			set :sass, Compass.sass_engine_options
		end
	end

	require_relative 'models'

	register Sinatra::Flash
	register Sinatra::Partial

	enable :method_override

	get '/' do
		haml :login, layout: :blank
	end

	get '/style.css' do
		sass :style
	end

	# not_found do
	# 	flash[:error] = "Not found"
	# 	redirect '/'
	# end

	helpers do

		def current?(path='')
			request.path_info=='/'+path ? 'current':  nil
		end

		def link_to_unless_current(url,text=url)
			link=request.path_info==url ? "" : "<a href=\"#{url}\">"
			link << text
			link << "</a>" unless request.path_info==url
			link
		end

  		def current_user
			@current_user
		end

		def authenticate!
			"401" unless current_user
		end
	end

end

require_relative 'routes/auth'
require_relative 'routes/tasks'
require_relative 'routes/tech'
require_relative 'routes/users'
require_relative 'routes/hardware'
require_relative 'routes/client'