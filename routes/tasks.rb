class Helpdesk < Sinatra::Base

	get '/dashboard', auth: [:support, :admin]  do 

		@title = 'Панель управления'
		
		@current_user = session[:user_id]

		@tasks = Task.all :order => :id.desc
		if @tasks.empty?
			flash[:error] = 'Заявок не найдено'
		end

		haml :dashboard
	end

	post '/task', auth: [:client, :support, :admin]  do 
		user = User.get params[:client_id]
		t = user.task.new
		t.name = params[:name]
		t.assignet_to = params[:assignet_to]
		t.content = params[:content]
		t.created_at = Time.now
		t.updated_at = Time.now
		if t.save
			flash[:notice] = 'Заявка успешна создана'
		else
			flash[:error] = 'Ошибка сохранения заявки'
		end
		if session[:role] == 'client'
			redirect '/account'
		else
			redirect '/dashboard'
		end 
	end

	get '/task/:id', auth: [:support, :admin]  do 
		@task = Task.get params[:id]
		if @task
			@title = "Просмотр #{@task.name}"
			haml :task_edit
		else
			flash[:error] = "Немогу найти заявку"
			redirect '/dashboard'
		end
	end

	put '/task/:id', auth: [:support, :admin]  do 
		t = Task.get params[:id]
		t.name = params[:name]
		t.assignet_to = params[:assignet_to]
		t.content = params[:content]
		t.completed = params[:completed] ? 1 : 0
		t.updated_at = Time.now
		if t.save
			flash[:notice] = 'Заявка обновлена'
			redirect '/dashboard'
		else
			flash[:error] = 'Ошибка обновления заявки'
			redirect '/dashboard'
		end
	end

	get '/task/:id/complete', auth: [:support, :admin]  do  
		t = Task.get params[:id]
		t.completed = t.completed ? 0 : 1 #flip it
		t.updated_at = Time.now
		if t.save
			flash[:notice] = 'Заявка отмечена как выполненныя'
			redirect '/dashboard'
		else
			flash[:error] = 'Возникла ошибка при попытке отметить заявку как выполненная'
			redirect '/dashboard'
		end
	end

	get '/task/:id/delete', auth: [:support, :admin]  do 
		@task = Task.get params[:id]
		@title = "Вы действительно хотите удалить заявку - #{@task.name}"
		if @task
			haml :task_delete, layout: :blank
		else
			flash[:error] = 'Немогу найти заявку'
			redirect '/dashboard'
		end
	end

	delete '/task/:id', auth: [:support, :admin]  do 
		t = Task.get params[:id]
		if t.destroy
			flash[:notice] = 'Заявка успешно удалена'
			redirect '/dashboard'
		else
			flash[:error] = 'Ошибка удаления заявки'
			redirect '/dashboard'
		end
	end

end