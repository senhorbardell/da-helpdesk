class Helpdesk < Sinatra::Base

	get '/tech', auth: [:support, :admin] do

		@title = 'Панель управления'

		@tasks = Task.all :order => :id.desc

		# user = User.get(1)
		# @user_hardware = user.hardware.all

		haml :tech
	end

end