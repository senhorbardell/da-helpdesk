class Helpdesk < Sinatra::Base

	get '/account', auth: [:client, :admin] do 
		user = User.get session[:user_id]
		@hardware = user.hardware.all
		@tasks = user.task.all order: :id.desc
		# admin = User.get(1)
		# @user_hardware = user.hardware.all
		@current_user = session[:user_id]
		# @tasks = admin.task.all order: :id.desc

		haml :account, layout: :client_layout
	end

end