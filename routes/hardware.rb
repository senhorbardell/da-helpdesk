class Helpdesk < Sinatra::Base

	get '/hardwares', auth: [:manager, :support, :admin] do

		@title = "Оборудование"

		@role = session[:role]

		@hardware = User.get(1).hardware.all

		haml :hardwares
	end

	get '/hardware/:id', auth: [:client, :admin] do

		@title = "Оборудование"

		@hardware = User.get(1).hardware.get params[:id]

		haml :hardware
	end

	put '/hardware/:id' do

	end

	post '/hardware/add' do 
		h = Hardware.new
		h.name = params[:name]
		h.type = params[:type]
		h.user_id = 1
		if h.save
			flash[:notice] = 'Оборудование успешно создано'
		else
			flash[:error] = 'Ошибка создания оборудования'
		end
		redirect '/hardwares'
	end

	get '/hardware/:id/delete' do 

	end

	delete '/hardware/:id' do 

	end

end