class Helpdesk < Sinatra::Base

	enable :sessions

	set(:auth) do |*roles|
		condition do
			unless is_authenticated? && roles.any? {|role| session[:role] == role.to_s}
				flash[:error] = "Unauthorized"
				redirect '/auth/login'
			end
		end
	end

	def in_role?(role)
		session[:role] == role
	end

	def require_logged_in
		redirect '/auth/login' unless is_authenticated?
	end

	def is_authenticated?
		return !!session[:user_id]
	end

	get '/admin', auth: :admin do 
		"Only admins are allowed here"
	end

	get '/auth/login' do
		@title = "Log in"
		haml :login, layout: :blank
	end

	post '/auth/login' do
		user = User.first(username: params['username'])
		if user.nil?
				flash[:error] = "The username you entered does not exist."
				redirect '/auth/login'
		elsif user.authenticate(params['password'])
			session[:user_id] = user.id
			session[:role] = user.role
			case user.role
			when 'admin'; redirect '/dashboard'
			when 'support'; redirect '/tech'
			when 'client'; redirect '/account'
			end
		else
			flash[:error] = "Could not log in"
			redirect '/auth/login'
		end
	end

	get '/auth/logout' do 
		session[:user_id] = nil
		session[:role] = nil
		flash[:notice] = 'Logged out'
		redirect '/auth/login'
	end

	get '/test/protected' do 
		require_logged_in
		"Secret"
	end

	get '/test/show' do 
		puts session[:user_id]
		puts session[:role]
	end

end