class Helpdesk < Sinatra::Base

	get '/users' do 
		# check_authentication
		@title = 'Список всех пользователей'
		@users = User.all
		haml :users
	end

	post '/user' do 
		# check_authentication
		u = User.new
		u.username = params[:username]
		u.password = params[:password]
		u.role = params[:role]
		if u.save
			flash[:notice] = 'Пользователь создан'
			redirect '/users'
		else
			flash[:error] = 'Ошибка создания пользователя'
			redirect '/users'
		end
	end

	get '/user/:id' do
		# check_authentication
		@user = User.get params[:id]
		if @user 
			@title = "Просмотр учетной записи #{@user.username}"
			haml :user
		else 
			flash[:error] = 'Пользователя не существует'
			redirect '/users'
		end
	end

	put '/user/:id/change' do 
		u = User.get params[:id]
		u.username = params[:username]
		u.role = params[:role]
		if u.save 
			flash[:notice] = 'Пользователь успешно обновлён'
			redirect '/users'
		else
			flash[:error] = 'Ошибка обновления пользователя'
			redirect '/users'
		end
	end

	put '/user/:id/reset' do
		# check_authentication
		u = User.get params[:id]
		if u.password == params[:old_password]
			u.password = params[:password]
			if u.save
				flash[:notice] = 'Пароль успешно обновлён.'
				redirect '/users'
			else
				flash[:error] = 'Ошибка обновления пароля'
				redirect '/users'
			end
		end
	end

	get '/user/:id/delete' do 
		# check_authentication
		@user = User.get params[:id]
		@title = "Подтвердите удаление #{@user[:username]}"
		if @user
			haml :user_delete, layout: :blank
		else
			flash[:error] = 'Немогу найти пользователя'
			redirect '/users'
		end
	end

	delete '/user/:id' do 
		# check_authentication
		u = User.get params[:id]
		if u.destroy
			flash[:notice] = 'Пользователь удалён'
			redirect '/users'
		else
			flash[:error] = 'Ошибка удаления пользователя'
			redirect '/users'
		end
	end
end