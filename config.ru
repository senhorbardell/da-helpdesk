require 'bundler'

if ENV['RACK_ENV'] == 'development'
	Bundler.require(:default, :development)
else
	Bundler.require(:default, :production)
end

require './main'
run Helpdesk